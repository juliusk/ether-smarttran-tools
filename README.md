A small (unmaintained) hobby project for learning rust macros and the Ethereum blockchain.

It contains utilities and a POC commandline client for querying smart contract transactions from the Ethereum blockchain.
The idea and some parts of the code is based on a really old version of ethabi (https://github.com/paritytech/ethabi).

`ether-smarttran-macro` is the main part, it contains a macro for generating smart contract transaction types ("events") build time
from a smart contract ABI (a JSON descriptor file) using. "Event filters" for each transaction type are also generated, for querying
transactions with specific data fields on the blockchain.

The console client connects to a locally running Ethereum node, or to `mainnet.infura.io` if an API key is given as argument.
It queries the latest Ethereum block id, the transactions in an Ethereum block with a given ID, and all ChainLink token smart contract
transfers between block 7657500 and 7657600.
