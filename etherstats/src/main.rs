extern crate rustc_hex;

extern crate hyper;
extern crate hyper_tls;

#[macro_use]
extern crate json;

extern crate ethabi;
#[macro_use]
extern crate ethabi_derive;
#[macro_use]
extern crate ethabi_contract;

extern crate ether_smarttran_core as contract;
#[macro_use]
extern crate ether_smarttran_macro;

extern crate bigint;

use std::env;

use std::str::FromStr;

use rustc_hex::{ToHex, FromHex};
use bigint::{U128, U256, Gas, Address, H256};

use hyper::{Client, Body};
use hyper::rt::{self, Future, Stream};
use hyper_tls::HttpsConnector;

use json::{JsonValue};

use contract::{Contract, Event, EventFilter};
use contract::{ToU256, ToH256, ToAddress};
mod erc20;
use erc20::{Erc20Contract, Erc20Args, TransferFilter, TransferEvent};
mod ethereum;
use ethereum::{EthereumClient, query_logs, current_blockid};

#[macro_use]
mod contract_macro;

contract_parser!(erc20_gen, ERC20, "src/eip20_abi.json");

// 10^9 wei
// can store approximately 18.45E9 eth, which is a fairly big number.
type gwei = u64;


trait ToGwei {
	fn to_gwei(&self) -> gwei;
}

impl<'a> ToGwei for &'a str {
	fn to_gwei(&self) -> gwei {
		self.to_u256().to_gwei()
	}
}

impl ToGwei for JsonValue {
	fn to_gwei(&self) -> gwei {
		self.as_str().unwrap().to_gwei()
	}
}

impl ToGwei for U128 {
	fn to_gwei(&self) -> gwei {
		(*self / U128::from(1_000_000_000)).as_u64() as gwei
	}
}

impl ToGwei for U256 {
	fn to_gwei(&self) -> gwei {
		(*self / U256::from(1_000_000_000)).as_u64() as gwei
	}
}

#[derive(Debug)]
enum Token {
	ETH(gwei),
	LINK(gwei)
}

#[derive(Debug)]
enum TransactionData {
	None,
	Transfer {
		from: String,
		to: String,
		token: Token
	},
	Other(String)
}

#[derive(Debug)]
struct Transaction<'a> {
	from: &'a str,
	to: &'a str,
	fee: gwei,
	hash: &'a str,
	block: u64,
	ether: gwei,
	data: TransactionData
}

fn parse_erc20_transfer(transaction_from: &str, args: Erc20Args) -> TransactionData {
	match args {
		Erc20Args::TransferMethodArgs { address, value } => TransactionData::Transfer {
			from: transaction_from.to_owned(),
			to: address,
			token: Token::LINK(value.to_gwei())
		},
		Erc20Args::TransferFromMethodArgs { from, to, value } => TransactionData::Transfer {
			from: from,
			to: to,
			token: Token::LINK(value.to_gwei())
		}
	}
}

fn parse_transaction_data(from: &str, to: &str, encoded: &str, contract: &Erc20Contract) -> TransactionData {
	if encoded == "0x" {
		TransactionData::None
	} else if contract.is(to) {
		parse_erc20_transfer(from, contract.parse_call(encoded).unwrap())
	} else {
		TransactionData::Other(encoded.to_owned())
	}
}

fn main() {
	println!("{:?}", TransferFilter::new(Some("0xada"), None).to_topic());
	//let encoding_contract = eip20::Eip20::default();
	//let encoded = encoding_contract.functions().total_supply().input();

	let args: Vec<String> = env::args().collect();

	let node_addr = if args.len() > 1 {
		// if the API key is given, use the public infura.io HTTPS endpoint
		format!("https://mainnet.infura.io/{}", args[1])
	} else {
		// else use the local node
		String::from("http://localhost:8545")
	};

	// 4 is number of blocking DNS threads
	let client = ethereum::EthereumClient::new(Client::builder().build::<_, hyper::Body>(HttpsConnector::new(4).unwrap()), node_addr);

	let contract = erc20::Erc20Contract::new("LINK", "0x514910771af9ca656af840dff83e8264ecf986ca");

	let req1 = ethereum::current_blockid(&client)
	.and_then(|json| {
		println!("{}", json.dump());
		Ok(())
	}).map_err(|e| {
		println!("{:?}", &e);
		()
	});

	/*let req2 = ethereum.call_contract("0x514910771af9ca656af840dff83e8264ecf986ca", &(encoded.to_hex()))
	.and_then(|json| {
		println!("{}", json.dump());
		return Ok(json);
	});*/

	let block_num = "0x437c1c"; // 4422684
	let block_number = U128::from_str(block_num).unwrap().as_u64();

	let req3 = ethereum::block_by_number(&client, block_num).and_then(move |j| {
		for t in j["result"]["transactions"].members() {
			let from = t["from"].as_str().unwrap();
			let to = t["to"].as_str().unwrap();

			let gas_price = t["gasPrice"].to_u256();
			let gas = t["gas"].to_u256();

			let transaction = Transaction {
				from: from,
				to: to,
				fee: (gas * gas_price).to_gwei(),
				hash: t["hash"].as_str().unwrap(),
				block: block_number,
				ether: t["value"].to_gwei(),
				data: parse_transaction_data(from, to, t["input"].as_str().unwrap(), &contract)
			};

			println!("{:?}", transaction);
		}

		Ok(())
	}).map_err(|e| {
		println!("{:?}", &e);
		()
	});

	let filter = erc20_gen::TransferEventFilter { from: None, to: None }; // TransferFilter::new(None, None)

	let req4 = ethereum::query_logs(&client, Some("0x514910771af9ca656af840dff83e8264ecf986ca"), Some(&filter), Some(7657500), Some(7657600))
	.and_then(|tr| {
		println!("{:?}", &tr);
		Ok(())
	}).map_err(|e| {
		println!("{:?}", &e);
		()
	});

	// run requests asynchronously
	rt::run(rt::lazy(move || {
		rt::spawn(req1);
		rt::spawn(req3);
		rt::spawn(req4);

		Ok(())
	}));
}
