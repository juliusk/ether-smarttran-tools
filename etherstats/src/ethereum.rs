use hyper::rt::{Future, Stream};
use hyper::client::connect::Connect;
use hyper::{Client, Request, Body};
use json::{JsonValue};
use contract::{Event, EventFilter};

pub struct EthereumClient<C: 'static + Connect> {
  https_client: Client<C,hyper::Body>,
  node_addr: String
}

impl<C: Connect> EthereumClient<C> {
  pub fn new(https_client: Client<C,hyper::Body>, node_addr: String) -> EthereumClient<C> {
    EthereumClient { https_client, node_addr }
  }
}

pub fn query<C: Connect>(client: &EthereumClient<C>, method: &str, data: JsonValue) -> impl Future<Item = json::JsonValue, Error = hyper::Error> {
  let req_body = object!{
    "jsonrpc" => "2.0",
    "method" => method,
    "params" => data,
    "id" => 1
  }.dump();

  println!("{} -> {}", client.node_addr, req_body);

  let mut req = Request::post(& client.node_addr) //::new(Method::Post, (& self.node_addr).parse().unwrap());
    .header("Content-Type", "application/json")
  .body(Body::from(req_body)).unwrap();

  //req.headers_mut().set(ContentLength(req_body.len() as u64));
  //req.set_body(req_body);

  return client.https_client.request(req).and_then(move |resp| {
    assert!(resp.status().is_success());
    println!("Response: {}", resp.status());

    return resp.into_body().concat2().map(move |body| {
      json::parse(std::str::from_utf8(&body).unwrap()).unwrap()
    });
  });
}

pub fn call_contract<C: Connect>(client: &EthereumClient<C>, contract_address: &str, encoded_call: &str) -> impl Future<Item = json::JsonValue, Error = hyper::Error> {
  query(client, "eth_call", array![object!{
    "to" => contract_address,
    "data" => format!("0x{}", encoded_call)
  }, "latest"])
}

pub fn query_logs<C: Connect, E: Event>(
  client: &EthereumClient<C>, address: Option<&str>, topic: Option<&EventFilter<E=E>>,
  from_block: Option<u64>, to_block: Option<u64>
) -> impl Future<Item = Vec<E>, Error = hyper::Error> {
  let mut params = json::object::Object::with_capacity(4);

  match address {
    Some(x) => params.insert("address", JsonValue::from(x)),
    None => {}
  }

  match from_block {
    Some(x) => params.insert("fromBlock", JsonValue::from(format!("{:#x}", x))),
    None => params.insert("fromBlock", JsonValue::from("latest"))
  }

  match to_block {
    Some(x) => params.insert("toBlock", JsonValue::from(format!("{:#x}", x))),
    None => params.insert("toBlock", JsonValue::from("latest"))
  }

  match topic {
    Some(x) => params.insert("topics", x.to_topic()),
    None => {}
  }

  query(client, "eth_getLogs", array![params]).map(|j| -> Vec<E> {
    println!("{}", j);

    j["result"].members()
      .filter(|log| {
        ! (log.has_key("removed") && log["removed"] == JsonValue::Boolean(true))
      })
      .map(|log| {
        println!("{}", log["transactionHash"].dump());
        println!("{}",log["topics"].dump());

        E::from(log)
      })
    .collect()
  })
}

pub fn current_blockid<C: Connect>(client: &EthereumClient<C>) -> impl Future<Item = json::JsonValue, Error = hyper::Error> {
  query(client, "eth_blockNumber", array![])
}

pub fn block_by_number<C: Connect>(client: &EthereumClient<C>, number: &str) -> impl Future<Item = json::JsonValue, Error = hyper::Error> {
  query(client, "eth_getBlockByNumber", array![number, true])
}