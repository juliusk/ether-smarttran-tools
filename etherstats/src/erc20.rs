/*
 * TODO: generate the content of this file from a JSON file, using macros like in ethabi
 */
use bigint::{H256, U256, Address};
use json::JsonValue;

use contract::{ToU256, ToH256, ToAddress};
use contract::{Contract, Function, Argument, ArgumentParser, AddressArgument, U256Argument, Event, EventFilter};

pub enum Erc20Args {
    TransferMethodArgs {
        address: String,
        value: U256
    },
    TransferFromMethodArgs {
        from: String,
        to: String,
        value: U256
    }
}

pub struct TransferMethod {}

impl Function<Erc20Args> for TransferMethod {
    fn name() -> &'static str {
        "transfer"
    }

    fn id() -> &'static str {
        // method_id(b"transfer(address,uint256)")
        "0xa9059cbb"
    }

    // [0-9]: method id
    // [10-73]: uint160 address encoded in uint256, left padded with zeros - real address is [34..74]
    // [74-137]: uint256 token value
    fn parse(encoded: &str) -> Erc20Args {
        let mut index = 10usize; // skip the method id

        let address = AddressArgument::parse(&encoded[index .. index + AddressArgument::len()]);
        index += AddressArgument::len();

        let value = U256Argument::parse(&encoded[index .. index + U256Argument::len()]);
        index += U256Argument::len();

        Erc20Args::TransferMethodArgs {
            address: address,
            value: value
        }
    }
}

pub struct TransferFromMethod {}

impl Function<Erc20Args> for TransferFromMethod {
    fn name() -> &'static str {
        "transferFrom"
    }

    fn id() -> &'static str {
        // id(b"transferFrom(address,address,uint256)")
        "0x23b872dd"
    }

    // [0-9]: method id
    // [10-73]: uint160 address encoded in uint256, left padded with zeros - real address is [34..74]
    // [74-137]: uint256 token value
    fn parse(encoded: &str) -> Erc20Args {
        let mut index = 10usize; // skip the method id

        let from = AddressArgument::parse(&encoded[index .. index + AddressArgument::len()]);
        index += AddressArgument::len();

        let to = AddressArgument::parse(&encoded[index .. index + AddressArgument::len()]);
        index += AddressArgument::len();

        let value = U256Argument::parse(&encoded[index .. index + U256Argument::len()]);
        index += U256Argument::len();

        Erc20Args::TransferFromMethodArgs {
            from: from,
            to: to,
            value: value
        }
    }
}

pub struct TransferFilter<'a> {
    from: Option<&'a str>,
    to: Option<&'a str>
}

impl<'a> TransferFilter<'a> {
    pub fn new(from: Option<&'a str>, to: Option<&'a str>) -> Self {
        TransferFilter { from, to }
    }
}

impl<'a> EventFilter for TransferFilter<'a> {
	type E = TransferEvent;
    fn to_topic(&self) -> JsonValue {
		// first parameter is keccak256 hash of the API event signature
        let mut result = vec![JsonValue::from("0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef")];
		
		// remaining optional parameters are the indexed, queryable arguments of the event
        match self.from {
            Some(x) => result.push(JsonValue::from(x)),
            None => {}
        }

        match self.to {
            Some(x) => result.push(JsonValue::from(x)),
            None => {}
        }

        return JsonValue::from(result);
    }
}

#[derive(Debug)]
pub struct TransferEvent {
    pub tr_hash: H256,
    pub contract: Address,
    pub from: Address,
    pub to: Address,
    pub value: U256
}

impl Event for TransferEvent {
	fn from(log: &JsonValue) -> Self {
		TransferEvent {
			tr_hash: log["transactionHash"].to_h256(),
			contract: log["address"].to_address(),
			from: log["topics"][1].to_address(),
			to: log["topics"][2].to_address(),
			value: log["data"].to_u256()
		}
	}
}

pub struct Erc20Contract {
    name: &'static str,
    address: &'static str
}

impl Contract<Erc20Args> for Erc20Contract {
    fn new(name: &'static str, address: &'static str) -> Self {
        Erc20Contract { name, address }
    }

    fn address(&self) -> &str {
        self.address
    }

    fn is(&self, address: &str) -> bool {
        address == self.address
    }

    fn parse_call(&self, encoded: &str) -> Result<Erc20Args,()> {
        if TransferMethod::is(encoded) {
            println!("{} ERC20 {}", self.name, TransferMethod::name());
            Ok(TransferMethod::parse(encoded))
        } else if TransferFromMethod::is(encoded) {
            println!("{} ERC20 {}", self.name, TransferFromMethod::name());
            Ok(TransferFromMethod::parse(encoded))
        } else {
            Err(())
        }
    }
}
