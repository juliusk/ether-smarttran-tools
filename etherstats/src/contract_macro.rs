#[macro_export]
macro_rules! contract_parser {
	($module: ident, $name: ident, $path: expr) => {
		#[allow(dead_code)]
		pub mod $module {
			use contract::{Contract, Function, Argument, ArgumentParser, AddressArgument, U256Argument, Event, EventFilter};
			use contract::{ToU256, ToH256, ToAddress};
			use bigint::{U256, H256, Address};
			use json::JsonValue;

			#[derive(ContractMsgParser)]
			#[abi_descriptor_path = $path]
            pub struct $name {
                name: &'static str,
                address: &'static str
            }
		}
	}
}
