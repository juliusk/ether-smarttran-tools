use std;
use json::JsonValue;
use std::str::FromStr;
use bigint::{U256, Address, H256};

/*
 * Conversion helpers
 */

pub trait ToU256 {
	fn to_u256(&self) -> U256;
}

impl<'a> ToU256 for &'a str {
	fn to_u256(&self) -> U256 {
		U256::from_str(self).unwrap()
	}
}

impl ToU256 for JsonValue {
	fn to_u256(&self) -> U256 {
		self.as_str().unwrap().to_u256()
	}
}

pub trait ToH256 {
	fn to_h256(&self) -> H256;
}

impl<'a> ToH256 for &'a str {
	fn to_h256(&self) -> H256 {
		H256::from_str(self).unwrap()
	}
}

impl ToH256 for JsonValue {
	fn to_h256(&self) -> H256 {
		self.as_str().unwrap().to_h256()
	}
}

pub trait ToAddress {
	fn to_address(&self) -> Address;
}

impl<'a> ToAddress for &'a str {
	fn to_address(&self) -> Address {
		if self.len() == 66 { // 256 bit hex string with '0x' prefix
			Address::from_str(&self[26..]).unwrap()
		} else {
			Address::from_str(self).unwrap()
		}
	}
}

impl ToAddress for JsonValue {
	fn to_address(&self) -> Address {
		self.as_str().unwrap().to_address()
	}
}

/*
 * Contract elements
 */

pub trait Argument {
	fn new(name: &'static str) -> Self;
	fn name(&self) -> &str;
	fn type_name() -> &'static str;
	fn len() -> usize;
}

pub trait ArgumentParser<T> {
	fn parse(encoded: &str) -> T;
}

pub struct AddressArgument {
	name: &'static str
}

impl Argument for AddressArgument {
	fn new(name: &'static str) -> Self {
		AddressArgument { name }
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn type_name() -> &'static str {
		"address"
	}

	fn len() -> usize {
		64
	}
}

impl ArgumentParser<String> for AddressArgument {
	fn parse(encoded: &str) -> String {
		format!("0x{}", &encoded[23..64])
	}
}

pub struct U256Argument {
	name: &'static str
}

impl Argument for U256Argument {
	fn new(name: &'static str) -> Self {
		U256Argument { name }
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn type_name() -> &'static str {
		"uint256"
	}

	fn len() -> usize {
		64
	}
}

impl ArgumentParser<U256> for U256Argument {
	fn parse(encoded: &str) -> U256 {
		U256::from_str(encoded).unwrap()
	}
}

pub trait Function<Args: Sized> {
	fn name() -> &'static str;
    fn id() -> &'static str;
	fn parse(encoded: &str) -> Args;

    fn is(encoded: &str) -> bool {
        encoded[2..10] == Self::id()[2..10]
    }
}

pub trait EventFilter {
	type E: Event;
    fn to_topic(&self) -> JsonValue;
}

pub trait Event {
	fn from(log: &JsonValue) -> Self;
}

pub trait Contract<Args: Sized> {
    fn new(name: &'static str, address: &'static str) -> Self;
    fn address(&self) -> &str;
    fn is(&self, address: &str) -> bool;
    fn parse_call(&self, encoded: &str) -> Result<Args,()>;
}

