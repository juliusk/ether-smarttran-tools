extern crate proc_macro;
extern crate ether_smarttran_core as contract;
#[macro_use]
extern crate json;
extern crate tiny_keccak;

extern crate syn;
#[macro_use]
extern crate quote;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use json::JsonValue;

use quote::Ident;

use proc_macro::TokenStream;

const ERROR_MSG: &'static str = "`derive(ContractMsgParser)` failed";

#[proc_macro_derive(ContractMsgParser, attributes(abi_descriptor_path))]
pub fn contract_msg_parser_derive(input: TokenStream) -> TokenStream {
	let s = input.to_string();
	let ast = syn::parse_derive_input(&s).expect(ERROR_MSG);
	let gen = contract_impl(&ast);
	println!("{}", gen.as_str());
	gen.parse().expect(ERROR_MSG)
}

fn contract_impl(ast: &syn::DeriveInput) -> quote::Tokens {
	let path = normalize_path(get_attribute(&ast.attrs, "abi_descriptor_path"));
    println!("abi_descriptor_path: {:?}", path);
    let abi = parse_abi_json(path);

	let contract_name = &ast.ident;
	let result_enum_name = Ident::from(format!("{}Result", contract_name));

	let result_enum = args_enum(&result_enum_name, &abi);

	let events = events(&abi);

	quote! {
		#events

		#result_enum

		impl Contract<#result_enum_name> for #contract_name {
			fn new(name: &'static str, address: &'static str) -> Self {
				#contract_name { name, address }
			}

			fn address(&self) -> &str {
				self.address
			}

			fn is(&self, address: &str) -> bool {
				address == self.address
			}

			fn parse_call(&self, msg: &str) -> Result<#result_enum_name,()> {
				Err(())
			}
		}
	}
}

fn args_enum(result_enum_name: &Ident, abi: &JsonValue) -> quote::Tokens {
	let result_types = abi.members().filter(|x| {
		abi_type(x) == "function"
		&& x["constant"].as_bool().unwrap() == false
	}).map(|x| args_enum_struct(x));

	quote! {
		pub enum #result_enum_name {
			#(#result_types),*
		}
	}
}

fn args_enum_struct(abi_item: &JsonValue) -> quote::Tokens {
	let result_name = Ident::from(capitalize_first(abi_item["name"].as_str().unwrap()));
	let fields = abi_item["inputs"].members().map(|x| arg_field(x, false)); // arg_field - no pub is needed for struct enums

	quote! {
		#result_name {
			#(#fields),*
		}
	}
}

fn arg_field(abi_arg_field: &JsonValue, is_optional: bool) -> quote::Tokens {
	let arg_name = Ident::from(abi_name(abi_arg_field));
	let arg_type = map_type(abi_type(abi_arg_field), is_optional);

	quote! {
		#arg_name: #arg_type
	}
}

fn struct_field(abi_arg_field: &JsonValue, is_optional: bool) -> quote::Tokens {
	let arg_name = Ident::from(abi_name(abi_arg_field));
	let arg_type = map_type(abi_type(abi_arg_field), is_optional);

	quote! {
		pub #arg_name: #arg_type
	}
}

fn events(abi: &JsonValue) -> quote::Tokens {
	let events = abi.members().filter(|x| {
		abi_type(x) == "event"
		&& x["anonymous"].as_bool().unwrap() == false
	}).map(|x| event(x));

	quote! {
		#(#events)*
	}
}

fn filter_indexed(event_field: &&JsonValue) -> bool {
	event_field["indexed"].as_bool().unwrap() == true
}

fn event(abi_event: &JsonValue) -> quote::Tokens {
	let name = abi_name(abi_event);
	let event_struct = Ident::from(format!("{}Event", name));
	let event_filter_struct = Ident::from(format!("{}EventFilter", name));

	let event_struct_fields = abi_event["inputs"].members()
		.map(|x| struct_field(x, false));

	let event_filter_struct_fields = abi_event["inputs"].members()
		.filter(filter_indexed)
		.map(|x| struct_field(x, true));

	let json_serialize_filter_fields = abi_event["inputs"].members()
		.filter(filter_indexed)
		.map(|x| {
			let field_name = Ident::from(abi_name(x));
			let field_type = abi_type(x);

			match field_type {
				"address" => quote! {
					match self.#field_name {
						Some(x) => result.push(JsonValue::from(format!("0x{}", x.hex()))),
						None => {}
					};
				},
				"uint256" => quote! {
					"fasz"
				},
				_ => panic!("Unknown type: {}", field_type)
			}
		});

	// indexed fields
	let mut json_parse_event_fields: Vec<quote::Tokens> = abi_event["inputs"].members()
		.filter(filter_indexed)
		.enumerate()
		.map(|(index, x)| {
			multi_field_parser_expr(abi_name(x), r#"log["topics"]"#, abi_type(x), index + 1)
		})
	.collect();

	// unindexe fields
	let json_parse_unindexed: Vec<_> = abi_event["inputs"].members()
		.filter(|x| !filter_indexed(x))
	.collect();

	if json_parse_unindexed.len() == 1 {
		let x = json_parse_unindexed[0];
		json_parse_event_fields.push(
			single_field_parser_expr(abi_name(x), r#"log["data"]"#, abi_type(x))
		);
	} else if json_parse_unindexed.len() > 1 {
		json_parse_event_fields.extend(
			json_parse_unindexed.iter()
				.enumerate()
				.map(|(index, x)| {
					multi_field_parser_expr(abi_name(x), r#"log["data"]"#, abi_type(x), index)
				})
		);
	}

	let event_id = event_id(abi_event);

	quote! {
		#[derive(Debug)]
		pub struct #event_struct {
			#(#event_struct_fields),*
		}

		#[derive(Debug)]
		pub struct #event_filter_struct {
			#(#event_filter_struct_fields),*
		}

		impl EventFilter for #event_filter_struct {
			type E = #event_struct;

		    fn to_topic(&self) -> JsonValue {
				// first parameter is keccak256 hash of the API event signature
				let mut result = vec![JsonValue::from(#event_id)];

				// remaining optional parameters are the indexed, queryable arguments of the event
				#(#json_serialize_filter_fields)*

				return JsonValue::from(result);
			}
		}

		impl Event for #event_struct {
			fn from(log: &JsonValue) -> Self {
				#event_struct {
					#(#json_parse_event_fields),*
				}
			}
		}
	}
}

fn abi_name<'a>(abi_arg_field: &'a JsonValue) -> &'a str {
	remove_leading_underscore(abi_arg_field["name"].as_str().unwrap())
}

fn abi_type(abi_arg_field: &JsonValue) -> &str {
	remove_leading_underscore(abi_arg_field["type"].as_str().unwrap())
}

fn map_type(type_name: &str, is_optional: bool) -> Ident {
	let assumed_type = match type_name {
		"address" => "Address",
		"uint256" => "U256",
		_ => panic!("Unknown type: {}", type_name)
	};

	let resolved_type = if is_optional {
		format!("Option<{}>", assumed_type)
	} else {
		assumed_type.to_owned()
	};

	Ident::from(resolved_type)
}

fn multi_field_parser_expr(field_name: &str, field_path: &str, field_type: &str, index: usize) -> quote::Tokens {
	single_field_parser_expr(field_name, &format!("{}[{}]", field_path, index), field_type)
}

fn single_field_parser_expr(field_name: &str, field_path: &str, field_type: &str) -> quote::Tokens {
	let field = Ident::from(field_path);
	let field_name_i = Ident::from(field_name);

	match field_type {
		"address" => quote! {
			#field_name_i: #field.to_address()
		},
		"uint256" => quote! {
			#field_name_i: #field.to_u256()
		},
		_ => panic!("Unknown type: {}", field_type)
	}
}

// based on: https://github.com/paritytech/ethabi/blob/d668b455584c3e52365619bba4e68c9790c36fcd/derive/src/lib.rs
fn get_attribute<'a>(attrs: &'a [syn::Attribute], name: &str) -> &'a str {
    let value = attrs.iter()
        .find(|a| a.name() == name)
        .map(|a| {
            match a.value {
                syn::MetaItem::NameValue(_, syn::Lit::Str(ref value, _)) => value,
                _ => panic!("Unknown value for attribute {}: {:?}", name, a)
            }
        });

    match value {
        Some(val) => val,
        None => panic!("Missing value for {}", name)
    }
}

// from: https://github.com/paritytech/ethabi/blob/d668b455584c3e52365619bba4e68c9790c36fcd/derive/src/lib.rs
fn normalize_path(relative_path: &str) -> PathBuf {
	// workaround for https://github.com/rust-lang/rust/issues/43860
	let cargo_toml_directory = std::env::var("CARGO_MANIFEST_DIR").unwrap();
	let mut path: PathBuf = cargo_toml_directory.into();
	path.push(relative_path);
	path
}

fn parse_abi_json(path: PathBuf) -> JsonValue {
	let mut content = String::new();
	File::open(path).unwrap().read_to_string(&mut content).unwrap();
	json::parse(&content).unwrap()
}

fn capitalize_first(s: &str) -> String {
	let mut char_indices = s.char_indices();
	let mut is_first = true;
	let mut out = String::with_capacity(s.len());

	while let Some((_, c)) = char_indices.next() {
		if is_first {
			out.extend(c.to_uppercase());
			is_first = false;
		} else {
			out.push(c);
		}
	}

	out
}

fn remove_leading_underscore(s: &str) -> &str {
	if s.starts_with('_') {
		&s[1..]
	} else {
		s
	}
}

// workaround because [u8] do not implement std::fmt::LowerHex
// and we cannot implement it directly on [u8] ...
struct ByteArr<'a>(&'a [u8]);
impl<'a> std::fmt::LowerHex for ByteArr<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		for byte in self.0.iter() {
            write!(f, "{:02x}", byte)?;
        }

        Ok(())
    }
}

fn method_id(method_signature: &[u8]) -> String {
	let digest = tiny_keccak::keccak256(method_signature);
	format!("0x{:x}",
		ByteArr(&digest[0..4])
	).to_owned()
}

fn encode_event_signature(event_signature: &[u8]) -> String {
	format!("0x{:x}",
		ByteArr(&tiny_keccak::keccak256(event_signature))
	).to_owned()
}

fn event_signature(abi_event: &JsonValue) -> String {
	let mut signature = String::from(abi_name(abi_event));
	signature.push('(');

	let mut is_first = true;
	for field in abi_event["inputs"].members() {
		if is_first {
			is_first = false;
		} else {
			signature.push(',');
		}
		signature.push_str(abi_type(field));
	}
	signature.push(')');

	signature
}

fn event_id(abi_event: &JsonValue) -> String {
	encode_event_signature(event_signature(abi_event).as_bytes())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn method_id_test() {
        println!("{}", method_id(b"transferFrom(address,address,uint256)"));
        println!("{}", encode_event_signature(b"Transfer(address,address,uint256)"));
    }

    #[test]
    fn event_id_test() {
    	let json = object! {
        	"anonymous" => false,
        	"inputs" => array! [
		        object! {
		            "indexed" => true,
		            "name" => "from",
		            "type" => "address"
		        },
		        object! {
		            "indexed" => true,
		            "name" => "to",
		            "type" => "address"
		        },
		        object! {
		            "indexed" => false,
		            "name" => "value",
		            "type" => "uint256"
		        }
		    ],
		    "name" => "Transfer",
		    "type" => "event"
		};

		assert_eq!(event_signature(&json), "Transfer(address,address,uint256)");
		assert_eq!(event_id(&json), "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef");
    }
}
